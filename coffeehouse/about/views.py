from django.shortcuts import render

# Create your views here.
def contact(request):
    #content of your request is extracted here.
    #and passed to the template for display.
    return render(request,'about/contact.html');

